package table_struct.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id"
      , nullable = false)
  private int id;

  @Column(name = "name"
      , nullable = false)
  private String name;

  @Column(name = "inn"
      , nullable = false)
  private long inn;

  @Column(name = "kpp"
      , nullable = false)
  private long kpp;

  @Column(name = "contact_name"
      , nullable = false)
  private String contactName;

  @Column(name = "telephone_number"
      , nullable = false)
  private String telephone;

  @Column(name = "address"
      , nullable = false)
  private String address;


}
