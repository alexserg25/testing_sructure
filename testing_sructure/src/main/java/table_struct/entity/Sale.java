package table_struct.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Sale {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id"
      , nullable = false)
  private int id;

  @Column(name = "sum"
      , nullable = false)
  private int sum;

  @Column(name = "customer_id"
      , nullable = false)
  private int customerId;

  @Column(name = "date_sale"
      , nullable = false)
  private Timestamp date;

}
