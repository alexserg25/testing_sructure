package table_struct.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class SaleItem {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id"
      , nullable = false)
  private int id;

  @Column(name = "product_id"
      , nullable = false)
  private int productId;

  @Column(name = "count"
      , nullable = false)
  private int count;

  @Column(name = "price"
      , nullable = false)
  private int price;

  @Column(name = "sale_id"
      , nullable = false)
  private int saleId;


}
